package image;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ImageBlur {

    public static void main(String[] args) {

        try {
            //Load images //
            BufferedImage twitter = ImageIO.read(new File("twitter.png"));
            BufferedImage strawberry = ImageIO.read(new File("strawberry.jpg"));

            //Blur Services //
            Blur blurer = new Blur();


            //Blur image //
            BufferedImage blurredImage = blurer.blur(twitter);

            // stop timer //

            //Write new blurred image //
            ImageIO.write(blurredImage, "jpg", new File("out.jpg"));

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
