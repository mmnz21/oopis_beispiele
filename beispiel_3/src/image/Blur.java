package image;

import java.awt.*;
import java.awt.image.BufferedImage;

class Blur {

    private float[][] blurmatrix = {
            {0.111f, 10.111f, 0.111f},
            {10.111f, 60.111f, 10.111f},
            {0.111f, 10.111f, 0.111f}};

    BufferedImage blur(BufferedImage image){

        BufferedImage newImage = new BufferedImage(image.getWidth(), image.getHeight(), image.getType());

        for(int x = 0; x <= image.getWidth()-1; x++){ // is x is the width
            for(int y = 0; y <= image.getHeight()-1; y++){ // y is the height

                int r = 0;
                int g = 0;
                int b = 0;

                // calculate new rgb value:

                for(int i = -1; i <= 1; i++){
                    for(int j = -1; j <= 1; j++){

                        if(x+i > 0 && x+i < image.getWidth()) { //check ranges  for width
                            if (y+j > 0 && y+j < image.getHeight()) { //check ranges for height

                                Color old = new Color(image.getRGB(x+i,y+j));

                                r += old.getRed() * (blurmatrix[i+1][j+1]/100);
                                g += old.getGreen() * (blurmatrix[i+1][j+1]/100);
                                b += old.getBlue() * (blurmatrix[i+1][j+1]/100);
                            }
                        }
                    }
                }

                Color value = new Color(r,g,b);
                newImage.setRGB(x,y,value.getRGB());
            }
        }

        return newImage;
    }
}
