package multi;

public class MultiHelloWorld {

    public static void main(String[] args) {

        SayHello hello1 = new SayHello("Bob");
        Thread t1 = new Thread(hello1);
        t1.start();

        SayHello hello2 = new SayHello("Alice");
        Thread t2 = new Thread(hello2);
        t2.start();

        SayHello hello3 = new SayHello("Tom");
        Thread t3 = new Thread(hello3);
        t3.start();
    }
}
