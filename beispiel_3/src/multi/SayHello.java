package multi;

import static java.lang.Thread.sleep;

class SayHello implements Runnable{

    private final String name;

    SayHello(String name) {
        this.name = name;
    }

    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {

        while(true) {
            System.out.println(name + " says hello");

            try {
                sleep(100); // 100 millis warten nach jeder Ausgabe
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}
