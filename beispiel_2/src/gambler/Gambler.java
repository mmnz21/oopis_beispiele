package gambler;

import dice.*;

public class Gambler {

    public static void main(String[] args){
        Dice[] dices = new Dice[5];

        dices[0] = new DiceD6();
        dices[1] = new DiceD20();
        dices[2] = new DiceD6();
        dices[3] = new DiceD6();
        dices[4] = new DiceD12();


        for (int i = 0; i < dices.length; i++) {
            System.out.println(dices[i].roll() + " out of " + dices[i].getDots());
        }
    }
}
