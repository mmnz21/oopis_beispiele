package dice;

import static java.lang.Math.floor;
import static java.lang.Math.random;

/**
 * Created by menzel on 5/2/17.
 */
public class DiceD12 implements Dice{

    private int dots = 12;

    @Override
    public int getDots() {
        return dots;
    }

    @Override
    public int roll() {
        return (int) floor(random() * dots + 1);
    }
}
