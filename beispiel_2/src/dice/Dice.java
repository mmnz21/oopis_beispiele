package dice;

public interface Dice {

    int getDots();

    int roll();
}
