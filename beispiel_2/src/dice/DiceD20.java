package dice;

import static java.lang.Math.*;

public class DiceD20 implements Dice{

    private int dots = 20;

    @Override
    public int getDots(){
        return this.dots;
    }

    @Override
    public int roll(){
        return (int) floor(random() * dots + 1);
    }

}
