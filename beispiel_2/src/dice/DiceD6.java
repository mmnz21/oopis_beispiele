package dice;

import static java.lang.Math.*;

public class DiceD6 implements Dice{

    private int dots = 6;

    @Override
    public int getDots(){
        return this.dots;
    }

    @Override
    public int roll(){
        return (int) floor(random() * dots + 1);
    }
}
