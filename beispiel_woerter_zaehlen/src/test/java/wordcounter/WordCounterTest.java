package wordcounter;

import org.junit.Test;
import wordcounter.calc.WordCounter;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * Created by menzel on 6/20/17.
 */
public class WordCounterTest {

    @Test
    public void countWords() throws Exception {
        String[] words = new String[5];
        words[0]  = "hallo";
        words[1]  = "hallo";
        words[2]  = "Haus";
        words[3]  = "Haus";
        words[4]  = "Nein";


        Map<String, Integer> counts = new HashMap();

        WordCounter wordCounter = new WordCounter();

        wordCounter.countWords(words);

        Map<String, Integer> expected = new HashMap();

        expected.put("hallo",2);
        expected.put("Haus",2);
        expected.put("Nein",1);


        assertEquals(expected,counts);


    }


}