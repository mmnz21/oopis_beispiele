package wordcounter.calc;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Count words inside a file
 *
 * Created by menzel on 3/13/17.
 */
public class WordCounter {

    /**
     * Counts how often each word occurs in the given array of strings
     *
     *
     * @param words - words to be counted
     * @return counted words as map
     */
    public Map<String, Integer> countWords(String[] words) {


        //filter words for not ATGC letters, line breaks etc.
        List<String> collect = Arrays.stream(words)
                        .map(String::toUpperCase)
                        .map(s -> s.replaceAll("[^ATGC]", ""))
                        .filter(s -> s.length() != 0)
                        .collect(Collectors.toList());

        //count the letters
        Map<String, Integer> counts = new HashMap<>();

        for(String word: collect){
            if(counts.containsKey(word))
                counts.put(word, counts.get(word)+1);
            else
                counts.put(word, 1);
        }

        //return result
        return counts;
    }
}
