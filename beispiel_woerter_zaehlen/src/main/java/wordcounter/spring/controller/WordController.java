package wordcounter.spring.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import wordcounter.calc.WordCounter;
import wordcounter.spring.command.Text;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by menzel on 7/7/17.
 */
@Controller
public class WordController {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String root(Model model) {

        model.addAttribute("text", new Text());

        return "input";
    }


    @RequestMapping(value = "/input", method = RequestMethod.GET)
    public String greeting(Model model) {

        model.addAttribute("text", new Text());

        return "input";
    }

    @RequestMapping(value = "/input", method = RequestMethod.POST)
    public String calc(@ModelAttribute Text text, Model model) {

        // get the text from the Text object
        String words = text.getContent();
        WordCounter wordCounter = new WordCounter();

        // send the words to wordCounter class for counting:
        Map<String, Integer> wordsMap = wordCounter.countWords(words.split(""));
        model.addAttribute("words", wordsMap);

        // calc percent values for colored bars:
        double sum = wordsMap.values().stream().mapToInt(Integer::intValue).sum();
        for(String key: wordsMap.keySet())
            model.addAttribute("key"+key, (wordsMap.get(key)/ sum)*100);

        return "result";
    }
}
