package wordcounter.spring.command;

/**
 * Created by menzel on 7/7/17.
 */
public class Text {

    private String content;

    public Text() {}

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
