package todo;

/**
 * Created by menzel on 6/7/17.
 */
public class Compute {

    public double bmi(int groesse, int gewicht){
        return gewicht / (Math.pow(groesse,2));
    }


    public double flaecheninhaltKreis(double radius){
        return  Math.PI * Math.pow(radius,2);
    }

}
