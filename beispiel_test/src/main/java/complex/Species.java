package complex;

/**
 * Created by menzel on 6/13/17.
 */
public class Species {
    private String genus;
    private String family;

    public Species(String genus, String family) {
        this.genus = genus;
        this.family = family;
    }

    public String getFamily() {
        return family;
    }

    public String getGenus() {
        return genus;
    }
}
