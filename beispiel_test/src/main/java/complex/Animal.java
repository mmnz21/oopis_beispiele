package complex;

/**
 * Created by menzel on 6/13/17.
 */
public class Animal {

    private Species species;
    private Area area;
    private String name;
    private int x;
    private int y;
    private double speed;

    public Animal(Species species, Area area, String name, int x, int y, double speed) {
        this.species = species;
        this.area = area;
        this.name = name;
        this.x = x;
        this.y = y;
        this.speed = speed;
    }

    public Species getSpecies() {
        return species;
    }

    public Area getArea() {
        return area;
    }

    public String getName() {
        return name;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x){
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public double getSpeed() {
        return speed;
    }
}
