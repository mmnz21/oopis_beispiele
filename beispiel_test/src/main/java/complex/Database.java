package complex;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by menzel on 6/13/17.
 */
public class Database {

    private List<Animal> animalList = new ArrayList<>();
    private List<Area> areaList = new ArrayList<>();

    public void addAnimal(Animal animal){
        this.animalList.add(animal);
    }

    public void addArea(Area area){
       this.areaList.add(area);
    }


    public List<Area> getAreaList() {
        return areaList;
    }

    public List<Animal> getAnimalList() {
        return animalList;
    }
}
