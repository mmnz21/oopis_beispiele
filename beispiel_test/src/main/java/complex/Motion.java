package complex;

import java.util.List;

/**
 * Created by menzel on 6/13/17.
 */
public class Motion {

    void moveAndCheck(Database forrest) {

        //move each animal
        forrest.getAnimalList().forEach(this::moveAnimal);

        //check collision
        checkEachAnimal(forrest);
    }

    void checkEachAnimal(Database forrest) {

        List<Animal> animals = forrest.getAnimalList();

        for(int i = 0; i < animals.size(); i++){
            for(int j = i; j < animals.size(); j++){

                Animal animal1 = animals.get(i);
                Animal animal2 = animals.get(j);

                if(isColliding(animal1, animal2, 2.5)){
                    System.out.println("collision of " + animal1.getName() + " and " + animal2.getName()
                            + " at x1:" + animal1.getX() + " y1:" + animal1.getY()
                            +  " x2:" + animal2.getX() + " y2:" + animal2.getY());
                }
            }
        }
    }

    boolean isColliding(Animal animal1, Animal animal2, double range) {

        return !animal1.equals(animal2) && (euclidean_distance(animal1.getX(), animal1.getY(), animal2.getX(), animal2.getY()) < range);
    }

    double euclidean_distance(int x1, int y1, int x2, int y2) {
        return Math.hypot(x1-x2, y1-y2);
    }

    void moveAnimal(Animal animal) {
            int x = animal.getX();
            int y = animal.getY();
            Area area = animal.getArea();

            x = (int) ((x + Math.random() * animal.getSpeed()) % area.getX2());
            y = (int) ((y + Math.random() * animal.getSpeed()) % area.getY2());

            animal.setX(x);
            animal.setY(y);
    }
}
