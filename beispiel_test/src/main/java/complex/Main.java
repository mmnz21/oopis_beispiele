package complex;

import java.util.List;

public class Main {


    public static void main(String args[]){
        Species dog = new Species("Canis", "Canidae");
        Species boar = new Species("Sus", "Suidae");
        Species blackrat = new Species("Rattus", "Muridae");

        Area northern_forrest = new Area(0,0,50,50);
        Database forrest = new Database();

        Animal bello = new Animal(dog, northern_forrest, "Bello", 15, 20, 10);
        Animal wuff = new Animal(dog, northern_forrest, "Wuff", 15, 1, 12);
        Animal grunz = new Animal(boar, northern_forrest, "Grunz", 5, 5, 5);
        Animal piep = new Animal(blackrat, northern_forrest, "Piep", 9, 7, 2);

        forrest.addArea(northern_forrest);

        forrest.addAnimal(bello);
        forrest.addAnimal(grunz);
        forrest.addAnimal(wuff);
        forrest.addAnimal(piep);

        Motion motion = new Motion();

        for(int i = 0; i < 200; i++)
            motion.moveAndCheck(forrest);
    }

}
