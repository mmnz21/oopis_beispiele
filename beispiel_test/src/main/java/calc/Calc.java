package calc;

/**
 * Created by menzel on 6/7/17.
 */
public class Calc {
    public static double sqrt(int number) {
        double t;

        double squareRoot = number / 2;

        do {
            t = squareRoot;
            squareRoot = (t + (number / t)) / 2;
        } while ((t - squareRoot) != 0);

	return squareRoot;
    }

}
