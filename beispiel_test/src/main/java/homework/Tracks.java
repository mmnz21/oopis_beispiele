package homework;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Collection of utils for track objects.
 * <p>
 * Created by Michael Menzel on 13/1/16.
 */
public final class Tracks {

    //prevent init of Intervals object with private constructor
    private Tracks() {
    }


    /**
     * TestTrack two intervals. Resulting track has only starts/stop where both input track were marked.
     *
     * @param intv1 - first input  track object
     * @param intv2 - second input track object
     * @return track with marked intervals were both input intervals were marked. Type is set to inout, names and scores get lost in intersect
     */
    public static Track intersect(Track intv1, Track intv2) {

        long[] starts1 = intv1.getStarts();
        long[] starts2 = intv2.getStarts();

        long[] ends1 = intv1.getEnds();
        long[] ends2 = intv2.getEnds();

        List<Long> result_start = new ArrayList<>();
        List<Long> result_end = new ArrayList<>();

        if (starts1.length > starts2.length) { //if start2 is smaller swap lists

            starts2 = intv1.getStarts();
            starts1 = intv2.getStarts();

            ends2 = intv1.getEnds();
            ends1 = intv2.getEnds();
        }

        // iterator through one of the intervals, check if a track in the other track is overlapping with the current. if not proceed if yes create new track
        int i2 = 0;

        for (int i1 = 0; i1 < starts1.length; i1++) {
            for (; i2 < ends2.length; i2++) {

                if (starts1[i1] < ends2[i2]) {
                    if (starts2[i2] < ends1[i1]) {
                        long start = Math.max(starts1[i1], starts2[i2]);
                        long end = Math.min(ends1[i1], ends2[i2]);

                        result_start.add(start);
                        result_end.add(end);

                    }
                }

                if (i1 < starts1.length - 1 && ends2[i2] > starts1[i1 + 1])
                    break;
            }
        }

        String name = intv1.getName() + "_" + intv2.getName();
        String desc = intv1.getDescription() + "_" + intv2.getDescription();

        return new InOutTrack(result_start,
                result_end,
                name,
                desc,
                intv1.getAssembly(),
                intv1.getCellLine());
    }


    /**
     * Sums up two intervals. The result has a track were any of the input intervals were marked
     *
     * @param intv1 - first track for sum
     * @param intv2 - second track for sum
     * @return sum of intv1 and intv2
     */
    public static Track sum(Track intv1, Track intv2) {
        return invert(intersect(invert(intv1), invert(intv2)));
    }

    /**
     * Xor of two intervals
     *
     * @param intv1 - first track
     * @param intv2 - second track
     * @return xor(interval1, interval2)
     */
    public static InOutTrack xor(Track intv1, Track intv2) {

        long[] starts1 = intv1.getStarts();
        long[] starts2 = intv2.getStarts();

        long[] ends1 = intv1.getEnds();
        long[] ends2 = intv2.getEnds();

        List<Long> result_start = new ArrayList<>();
        List<Long> result_end = new ArrayList<>();
        long previousEnd = 0;

        int j = 0;
        int i = 0;

        for (; i < starts1.length; i++) {
            long start = starts1[i];
            long end = ends1[i];
            long nextStart;
            boolean s = false;

            if (start > starts2[j]) {
                s = true;
            }

            if (s && j < starts2.length - 1) {
                nextStart = starts1[i];
                start = starts2[j];
                end = ends2[j++];

            } else {
                nextStart = starts2[j];
            }

            if (start < previousEnd) {
                start = previousEnd;
            }

            previousEnd = end;

            if (end > nextStart) {
                end = nextStart;
            }


            if (end != start) {
                result_start.add(start);
                result_end.add(end);
            }

            if (s) i--;

        }

        String name = intv1.getName() + "_" + intv2.getName();
        String desc = intv1.getDescription() + "_" + intv2.getDescription();

        return new InOutTrack(result_start, result_end, name, desc, intv1.getAssembly(), intv2.getCellLine());
    }


    /**
     * Sums up the size of all intervals of a given track
     *
     * @param track - track with the intervals to sum up
     * @return sum of track length inside the intervals
     */
    public static long sumOfIntervals(Track track) {

        long size = 0;

        long[] intervalStart = track.getStarts();
        long[] intervalEnd = track.getEnds();

        for (int i = 0; i < intervalStart.length-1; i++)
            size += intervalEnd[i] - 1 - intervalStart[i];

        return size;
    }


    /**
     * Inverts track. Scored and named intervals loose their Type because scores and names cannot be kept.
     *
     * @param track - track to invert
     * @return inverted track
     */
    public static InOutTrack invert(Track track) {

        if (track.getStarts().length == 0)
            return (InOutTrack) track.clone();

        // copy start to end and end to start list
        List<Long> starts = Arrays.stream(track.getEnds()).boxed().collect(Collectors.toList());
        List<Long> ends = Arrays.stream(track.getStarts()).boxed().collect(Collectors.toList());

        if (track.getStarts()[0] != 0L)
            starts.add(0, 0L);
        else ends.remove(0);


        if (track.getEnds()[track.getEnds().length - 1] ==  3137161264L)
            starts.remove(starts.size() - 1);
        else
            ends.add(3137161264L);

        return new InOutTrack(starts, ends, track.getName(), track.getDescription(), track.getAssembly(), track.getCellLine());
    }



    /**
     * Test if the intervals of a track are correct.
     * Correct means in order. not overlapping and the start and end list are of same size.
     *
     * @param track - track to test
     * @return true if track is good. false if not.
     *
     */
    public static boolean checkTrack(Track track){

        long[] intervalStart = track.getStarts();
        long[] intervalEnd = track.getEnds();

        if (intervalEnd.length != intervalStart.length) return false;

        Long lastStart = 0L;
        Long lastEnd = 0L;

        for (int i = 0; i < intervalEnd.length; i++) {
            Long start = intervalStart[i];
            Long end = intervalEnd[i];

            if(start > end) return false;
            if(start < lastEnd) return false;
            if(lastStart > start) return false;

            lastEnd = end;
            lastStart = start;
        }

        return true;
    }
}
