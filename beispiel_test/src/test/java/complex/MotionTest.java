package complex;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by menzel on 6/13/17.
 */
public class MotionTest {

    @Test
    public void checkEachAnimal() throws Exception {
       //TODO
    }

    private Motion motion = new Motion();

    @Test
    public void isColliding() throws Exception {
        Animal first = mock(Animal.class);
        Animal second = mock(Animal.class);

        when(first.getX()).thenReturn(5);
        when(second.getX()).thenReturn(5);

        when(first.getY()).thenReturn(3);
        when(second.getY()).thenReturn(4);

        assertTrue(motion.isColliding(first,second,2));
    }

    @Test
    public void move() throws Exception {

        Area area = mock(Area.class);
        when(area.getX1()).thenReturn(10);
        when(area.getX2()).thenReturn(10);

        Animal animal = new Animal(null, area, "Name", 5, 3, 12);

        motion.moveAnimal(animal);

        // moved the animal and test if the position has changed
        assertTrue(animal.getX() != 5);
        assertTrue(animal.getY() != 3);

    }

}