package calc;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by menzel on 6/7/17.
 */
class CalcTest {

    void foo(){

    }

    @Test
    void testSQRT55() {

        assertEquals(8.4161, Calc.sqrt(55), 0.01);
    }

    @Test
    void testSQRT() {


        for(int i = 2; i < 10; i++){
            assertEquals(Math.sqrt(i), Calc.sqrt(i), 0.00001);
        }
    }

}