package homework;

import org.junit.Test;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import static org.junit.Assert.assertArrayEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TracksTest {

    @Test
    public void intersect() throws Exception {
        Track first = mock(Track.class);
        when(first.getStarts()).thenReturn(new long[]{5L,15L,22L});
        when(first.getEnds()).thenReturn(new long[]{10L,20L,30L});


        Track second = mock(Track.class);
        when(second.getStarts()).thenReturn(new long[]{5L,12L,19L});
        when(second.getEnds()).thenReturn(new long[]{9L,17L,28L});

        Track result = Tracks.intersect(first, second);

        assertArrayEquals(new long[]{5L,15L,19L, 22L}, result.getStarts());
        assertArrayEquals(new long[]{9L,17L,20L,28L}, result.getEnds());
    }

    @Test
    public void sum() throws Exception {
        //TODO
        throw new NotImplementedException();
    }

    @Test
    public void xor() throws Exception {
        //TODO
        throw new NotImplementedException();
    }

    @Test
    public void sumOfIntervals() throws Exception {
        //TODO
        throw new NotImplementedException();
    }

    @Test
    public void invert() throws Exception {
        //TODO
        throw new NotImplementedException();
    }

    @Test
    public void checkTrack() throws Exception {
        //TODO
        throw new NotImplementedException();
    }

}