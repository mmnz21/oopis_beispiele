package main;

import auftraege.Auftrag;
import auftraege.Auftragsverwaltung;
import kunden.Kunde;
import kunden.Privatkunde;
import kunden.Kundenverwaltung;

import java.util.List;
import java.util.Random;

/**
 * Created by menzel on 3/8/17.
 */
public class Main {

   public static void main(String[] args) {

       // kunden erstellen

      int anzahlKunden = 10;
      Kundenverwaltung kundenverwaltung = new Kundenverwaltung();

      for(int i =0; i< anzahlKunden; i++){
         Privatkunde kunde = new Privatkunde("Hans " + i, "Berlin");
         kunde.setKontostand(Math.random()*1000);
         kundenverwaltung.kunde_hinzufuegen(kunde);
      }

      kundenverwaltung.printKunden();
      kundenverwaltung.printSumme();


      // Aufträge erstellen

      Auftragsverwaltung auftragsverwaltung = new Auftragsverwaltung();
      List<Kunde> kunden = kundenverwaltung.alle_kunden();
      Random random = new Random();


      for(int i =0; i< 100; i++){
         Auftrag auftrag = new Auftrag(kunden.get(Math.abs(random.nextInt()%anzahlKunden)), kunden.get(Math.abs(random.nextInt()%anzahlKunden)), Math.random()*100);

         if(auftrag.getKunde1() != auftrag.getKunde2())
            auftragsverwaltung.addAuftrag(auftrag);
            //System.out.println(auftrag.getKunde1().getName() + " zu " + auftrag.getKunde2().getName() + ": " + auftrag.getBetrag());
      }

      //alles abarbeiten
      auftragsverwaltung.auftraegeAbarbeiten();

      System.out.println("----------");
      kundenverwaltung.printKunden();
      kundenverwaltung.printSumme();


   }
}
