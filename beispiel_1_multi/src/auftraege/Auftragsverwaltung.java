package auftraege;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * Created by menzel on 3/16/17.
 */
public class Auftragsverwaltung {

    private final ExecutorService exe;
    private int threadCount = 32;
    private List<Auftrag> auftragList;


    public Auftragsverwaltung() {

        BlockingQueue<Runnable> queue = new ArrayBlockingQueue<>(1024);
        exe = new ThreadPoolExecutor(4, threadCount, 30L, TimeUnit.SECONDS, queue);
        auftragList = new ArrayList<>();
    }

    public void auftraegeAbarbeiten(){

        for(Auftrag auftrag: auftragList) {
            exe.execute(new Auftragsabarbeiter(auftrag));
        }

        exe.shutdown();

        try {
            exe.awaitTermination(1, TimeUnit.MINUTES);

        } catch (InterruptedException e) {
            e.printStackTrace();
            exe.shutdownNow();
        } finally {
            if (!exe.isTerminated())
                System.err.println("Killing all tasks now");
            exe.shutdownNow();
        }

        System.err.println("Not done: " + auftragList.stream().filter(auftrag -> !auftrag.isDone()).count());
    }

    private class Auftragsabarbeiter implements Runnable{

        private Auftrag auftrag;

        private Auftragsabarbeiter(Auftrag auftrag) {

            this.auftrag = auftrag;
        }

        @Override
        public void run() {


            double betrag = auftrag.getBetrag();

            boolean erlaubt;
            double stand1 = auftrag.getKunde1().getKontostand();
            erlaubt = stand1 - betrag > 0;

            try {
                Thread.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if(erlaubt) {
                auftrag.getKunde1().setKontostand(stand1 - betrag);

                double stand2 = auftrag.getKunde2().getKontostand();
                auftrag.getKunde2().setKontostand(stand2 + betrag);
            }

            auftrag.setDone();


        }
    }


    public void addAuftrag(Auftrag auftrag){
        this.auftragList.add(auftrag);
    }
}
