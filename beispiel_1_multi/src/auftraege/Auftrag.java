package auftraege;

import kunden.Kunde;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by menzel on 3/16/17.
 */
public class Auftrag {

    private final Kunde kunde1;
    private final Kunde kunde2;
    private final double betrag;
    private boolean done;

    public Auftrag(Kunde kunde1, Kunde kunde2, double betrag) {
        this.kunde1 = kunde1;
        this.kunde2 = kunde2;
        this.betrag = betrag;
        this.done = false;
    }

    public synchronized Kunde getKunde1() {
        return kunde1;
    }

    public synchronized Kunde getKunde2() {
        return kunde2;
    }

    public double getBetrag() {
        return betrag;
    }

    public void setDone(){
        this.done = true;
    }

    public boolean isDone() {
        return done;
    }

}
