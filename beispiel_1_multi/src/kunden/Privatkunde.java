package kunden;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Ein einzelner Kunde
 *
 * Created by menzel on 3/8/17.
 */
public class Privatkunde implements Kunde{
    private String name;
    private String adresse;
    private double kontostand;
    private Lock lock;

    public Privatkunde(String name, String adresse) {
        this.name = name;
        this.adresse = adresse;
        kontostand = 0;
        this.lock = new ReentrantLock();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    @Override
    public double getKontostand() {
        return kontostand;
    }

    @Override
    public void setKontostand(double kontostand) {
        this.kontostand = kontostand;
    }

    @Override
    public synchronized Lock getLock() {
        return this.lock;
    }


}
