package kunden;

import java.util.concurrent.locks.Lock;

/**
 * Created by menzel on 3/16/17.
 */
public interface Kunde {
    String getName();

    String getAdresse();

    double getKontostand();

    void setKontostand(double betrag);

    Lock getLock();
}
