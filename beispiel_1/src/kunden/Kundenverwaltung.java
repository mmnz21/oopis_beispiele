package kunden;

import java.util.ArrayList;
import java.util.List;

/**
 * Verwaltung für alle Kunden
 *
 * Created by menzel on 3/8/17.
 */
public class Kundenverwaltung {

    private List<Kunde> kunden;

    public Kundenverwaltung(){
        kunden = new ArrayList<>();
    }

    public void kunde_hinzufuegen(Kunde kunde){
        this.kunden.add(kunde);
    }

    public void kunde_entfernen(Kunde kunde){
        this.kunden.remove(kunde);
    }

    public List<Kunde> alle_kunden(){ return this.kunden; }
}
