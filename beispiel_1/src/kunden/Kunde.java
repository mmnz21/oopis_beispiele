package kunden;

/**
 * Ein einzelner Kunde
 *
 * Created by menzel on 3/8/17.
 */
public class Kunde {
    private String name;
    private String adresse;
    private double kontostand;

    public Kunde(String name, String adresse) {
        this.name = name;
        this.adresse = adresse;
        kontostand = 0;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public double getKontostand() {
        return kontostand;
    }

    public void setKontostand(double kontostand) {
        this.kontostand = kontostand;
    }
}
