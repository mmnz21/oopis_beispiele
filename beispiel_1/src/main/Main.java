package main;

import java.util.Arrays;

/**
 * Berechnet Zinssätze für 3 verschiedene Kapitalanlagen
 *
 * Created by menzel on 3/8/17.
 */
public class Main {

    public enum Apple {GRANNY_SMITH, RED_DELICIOUS}
    public enum Banana {GROS,_MICHEL, CAVENDISH, CHIQUITA, RED}

    public enum Operation {
        PLUS("+") {double apply(double x, double y){return x + y;}},
        MINUS("+") {double apply(double x, double y){return x - y;}},
        TIMES("+") {double apply(double x, double y){return x * y;}},
        DIVIDE("+") {double apply(double x, double y){return x / y;}};

        abstract double apply(double x, double y);
        private final String symbol;

        Operation(String symbol){this.symbol = symbol;}

        @Override
        public String toString(){ return symbol;}
    }

   public static void main(String[] args) {

       Operation op = Operation.PLUS;
       double x = 5;
       double y = 5;

       double result = op.apply(x,y);

       System.out.println(x + " " + op + " " + y + " = "+ result);

   }

   /**
    * Gibt die Werte der Zinsberechnung aus.
    *
    * @param kapital - Startkapital der Anlage
    * @param laufzeit - Zinssatz der Anlage
    * @param endKap -
    */
   private static void ausgabe(double kapital, int laufzeit, double zins, double endKap) {

      System.out.println("Start mit " + kapital + " Euro");
      System.out.println("Ergibt bei einer Laufzeit von " + laufzeit + " Jahren und einem Zinssatz von " + zins + " Prozent");
      System.out.println("Endkapital von " + endKap);
      System.out.println();

   }

   /**
    * Berechnet das Kaptial einer Anlage mit dem Startwert kapital zu einem Zinssatz
    * der Höhe zins und einer Laufzeit in Jahren laufzeit.
    *
    * @param kapital - Startkapital der Anlage
    * @param zins - Zinssatz der Anlage
    * @param laufzeit - Laufzeit der Anlage
    *
    * @return Wert der gesamten Anlage (kapital + Zinseszins)
    */
   private static double zinsen_berechnen(double kapital, double zins, int laufzeit) {
         double endKap, q;

         q = 1 + zins/100;
         endKap = kapital * Math.pow(q, laufzeit);  // Berechnung
         endKap  = Math.rint(endKap*100);   // Rundung
         endKap = endKap/100;


         return endKap;
   }
}
