package main;

import db.admin.DatabaseQuery;
import db.admin.local.DatabaseQueryLocal;
import db.io.FileReader;
import db.sample.Protein;

import java.util.stream.Collectors;

/**
 * Created by menzel on 7/13/17.
 */
public class Main {

    public static void main(String[] args) {
        new FileReader(); // lädt die Dateien in die Datenbank
        DatabaseQuery query = new DatabaseQueryLocal();

        query.getProteins().forEach(p -> System.out.print(p.getIdentifier() + ", "));
        query.getGenes().forEach(p -> System.out.println(p.getIdentifier() + "\n" + p.getSequence() + "\n"));
        query.getGenes().forEach(g -> System.out.println(g.getProtein().getIdentifier()));
        query.getAssays().forEach(g -> System.out.println(g.getUsedProteins().stream().map(Protein::getIdentifier).collect(Collectors.toList())));

    }
}
