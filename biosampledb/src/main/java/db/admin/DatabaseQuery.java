package db.admin;

import db.sample.Assay;
import db.sample.Gene;
import db.sample.Protein;
import org.apache.commons.lang3.tuple.Pair;

import java.util.List;
import java.util.Optional;

/**
 * Interface for any database
 *
 * Created by menzel on 7/14/17.
 */
public interface DatabaseQuery {
    List<Assay> getAssays();

    List<Gene> getGenes();

    List<Protein> getProteins();

    void addAssay(Assay assay);

    void addGene(Gene gene);

    void addProtein(Protein protein);

    /**
     * Returns all Assays that used a Protein that is associated with the given gene.
     *
     * @param gene - gene to get the proteins
     *
     * @return list of associated assays
     */
    List<Assay> getAssaysByGene(Gene gene);

    /**
     * Returns all Proteins that have a measurement above or equal to 'meas' in any Assay
     *
     * @param meas - threshold value
     * @return A list of tuples (Pairs) where the Protein is left and the measured score from the assay is right
     */
    List<Pair<Protein, Double>> getAssayResultsByMeas(Double meas);

    /**
     * Return all Scores and assoc. Assays for a given Protein
     *
     * @param protein - protein to look up
     * @return List of Pairs of assays and results for the given protein on that assay
     */
    List<Pair<Assay, Double>> getScores(Protein protein);

    /**
     * Returns a protein by given identifier. Returns Optinal.empty if there is no protein with this name
     *
     * @param identifier - id of the protein
     * @return Optional protein
     */
    Optional<Protein> getProteinByName(String identifier);

    /**
     * Returns a gene by given identifier. Returns Optinal.empty if there is no gene with this name
     *
     * @param name - id  of a gene
     * @return Optional protein
     */
    Optional<Gene> getGeneByName(String name);

    /**
     * Returns a assay by given identifier. Returns Optinal.empty if there is no assay with this name
     *
     * @param name - id  of a gene
     * @return Optional protein
     */
    Optional<Assay> getAssayByName(String name);
}
