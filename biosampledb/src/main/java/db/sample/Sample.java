package db.sample;

import java.util.UUID;

/**
 * Superclass for all samples
 *
 * Created by menzel on 7/13/17.
 */
public abstract class Sample {

    UUID id;

    public UUID getId() {
        return id;
    }


}
