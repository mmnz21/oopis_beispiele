package db.sample;

import java.util.List;
import java.util.UUID;

/**
 * Gene
 *
 * Created by menzel on 7/13/17.
 */
public class Gene extends Sample{

    private final String identifier;
    private final Protein protein;
    private final String sequence;
    private final String desc;

    Gene(UUID id, String identifier, String desc, Protein protein, String sequence) {
        this.desc = desc;
        this.id = id;
        this.identifier = identifier;
        this.protein = protein;
        this.sequence = sequence;
    }

    public String getIdentifier() {
        return identifier;
    }

    public Protein getProtein() {
        return protein;
    }

    public String getSequence() {
        return sequence;
    }

    public String getDesc() {
        return desc;
    }
}
