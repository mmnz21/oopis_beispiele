package db.sample;

import java.util.UUID;

/**
 * Protein
 *
 * Created by menzel on 7/13/17.
 */
public class Protein extends Sample{

    private final String identifier;
    private final String desc;
    private final String sequence;

    Protein(UUID id, String identifier, String desc, String sequence) {
        this.desc = desc;
        this.sequence = sequence;
        this.id = id;
        this.identifier = identifier;
    }

    public String getIdentifier() {
        return identifier;
    }

    public String getDesc() {
        return desc;
    }

    public String getSequence() {
        return sequence;
    }
}
