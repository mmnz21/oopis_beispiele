package db.sample;

import java.util.List;
import java.util.UUID;

/**
 * Assay
 *
 * Created by menzel on 7/13/17.
 */
public class Assay extends Sample{

    private final List<Protein> usedProteins;
    private final List<Double> measurements;
    private String name;

    Assay(UUID id, String name, List<Protein> usedProteins, List<Double> measurements) {
        this.name = name;
        this.id = id;
        this.usedProteins = usedProteins;
        this.measurements = measurements;
    }

    public List<Protein> getUsedProteins() {
        return usedProteins;
    }

    public List<Double> getMeasurements() {
        return measurements;
    }

    public String getName() {
        return name;
    }
}

