package db.sample;

import db.admin.DatabaseLocal;
import db.admin.local.DatabaseQueryLocal;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * Factory for samples
 * Created by menzel on 7/13/17.
 */
public class SampleFactory {

    public Assay createAssay(List<Protein> proteins, List<Double> measurements){

        UUID uuid = UUID.randomUUID();
        return new Assay(uuid, "as:"+uuid.toString().substring(8), proteins,measurements);
    }


    public Gene createGene(String identifier, String desc, String proteinName, String sequence){

        Optional<Protein> protein = new DatabaseQueryLocal(DatabaseLocal.getInstance()).getProteinByName(proteinName);

        if(!protein.isPresent())
            System.err.println("No protein found with that name: " + proteinName + ". By gene: " + identifier);

        UUID uuid = UUID.randomUUID();
        return new Gene(uuid,identifier, desc, protein.get() ,sequence);
    }

    public Protein createProtein(String identifier, String desc, String sequence){

        UUID uuid = UUID.randomUUID();
        return new Protein(uuid,identifier, desc, sequence);
    }

}
