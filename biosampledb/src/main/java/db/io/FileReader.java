package db.io;

import db.admin.DatabaseLocal;
import db.admin.DatabaseQuery;
import db.admin.local.DatabaseQueryLocal;
import db.sample.Protein;
import db.sample.SampleFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Reads the db files (with magic)
 *
 * Created by menzel on 7/13/17.
 */
public class FileReader {
    SampleFactory factory = new SampleFactory();
    DatabaseLocal db = DatabaseLocal.getInstance();
    DatabaseQuery query = new DatabaseQueryLocal(db);


    public FileReader() {
        parseProteins(new File("uniprot_small.fasta").toPath());
        parseGenes(new File("ref").toPath(), new File("genes.fasta").toPath());
        parseAssay(new File("assays.tab").toPath());
    }

    /**
     * Parses a given fasta file which should contains proteins in fasta format.
     *
     * @param file to parse
     */
    public void parseProteins(Path file){

        try (Stream<String> stream = Files.lines(file)) {
            final String[] identifier = new String[1];
            final String[] desc = new String[1];
            final String[] sequence = {""};

            stream.forEach(line -> {

                if(line.startsWith(">")){

                    if(sequence[0].length() > 0){
                        db.addProtein(factory.createProtein(identifier[0],desc[0], sequence[0]));
                        identifier[0] = "";
                        desc[0] = "";
                        sequence[0] = "";
                    }

                    String[] parts = line.split("\\|");
                    identifier[0] = parts[1];
                    desc[0] = parts[2];
                } else {
                    sequence[0] += line.trim();
                }


            });

            if(sequence[0].length() > 0){
                db.addProtein(factory.createProtein(identifier[0],desc[0], sequence[0]));
            }


        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void parseGenes(Path refsFile, Path genes){
        // parse refs first
        Map<String, String> refs = new HashMap<>();


        try (Stream<String> stream = Files.lines(refsFile)) {
            stream.forEach(line -> {
                refs.put(line.split("\\s+")[0], line.split("\\s+")[1]);
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (Stream<String> stream = Files.lines(genes)) {
            final String[] identifier = new String[1];
            final String[] desc = new String[1];
            final String[] sequence = {""};

            stream.forEach(line -> {

                if(line.startsWith(">")){

                    if(sequence[0].length() > 0){
                        db.addGene(factory.createGene(identifier[0],desc[0],refs.get(identifier[0]),sequence[0]));
                        identifier[0] = "";
                        desc[0] = "";
                        sequence[0] = "";
                    }

                    String[] parts = line.split(":");
                    identifier[0] = parts[0].substring(1);
                    desc[0] = parts[1];
                } else {
                    sequence[0] += line.trim();
                }
            });

            if(sequence[0].length() > 0){
                db.addGene(factory.createGene(identifier[0],desc[0],refs.get(identifier[0]),sequence[0]));
            }


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void parseAssay(Path file){

        try (Stream<String> stream = Files.lines(file)) {


            stream.forEach(line -> {
                List<Protein> ids =  Arrays.stream(line.split(";")[0].split(","))
                        .map(name -> query.getProteinByName(name.trim()))
                        .filter(Optional::isPresent)
                        .map(Optional::get)
                        .collect(Collectors.toList());

                List<Double> scores = Arrays.stream(line.split(";")[1].split(","))
                        .map(Double::parseDouble)
                        .collect(Collectors.toList());

                if(ids.size() != scores.size()){
                    System.err.println("Sizes of assays score lists differ at " + line);
                }

                db.addAssay(factory.createAssay(ids, scores));
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
