package gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.Timer;
import javax.swing.WindowConstants;

import client.Client;

public class ClientGUI extends JFrame {

	private static final long serialVersionUID = 1598816681569203287L;
	private static Client client;
	private static JTextField IPin;
	private static JTextField PORTin;
	private static JTextField NICKin;
	private static JButton mainButton;
	private static JButton sendButton;
	private static JTextArea CHAT;
	private static JTextField msgField;

	private static DefaultListModel<String> listModel;
	private static JList<String> userlist;

	public ClientGUI() {
		super("Chat Client");
		listModel = new DefaultListModel<String>();
		userlist = new JList<String>(listModel);
		
		initGUI();
	}

	private void initGUI() {
		setSize(700, 480);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {

			public void windowClosing(WindowEvent e) {
				clientDisconnect(false);
			}
		});

		mainButton = createButton("Connect");
		buttonListener(mainButton);

		sendButton = createButton("Send");
		buttonListener(sendButton);

		userlist.setFixedCellWidth(150);

		NICKin = createTextField("NICK_field", 15, true);
		IPin = createTextField("IP_field", 12, true);
		PORTin = createTextField("PORT_field", 6, true);
		msgField = createTextField("MSG_Field", 50, true);
		
		msgField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if ((client != null) && (msgField.getText().length() != 0) && client.isConnected()) {
					Client.sendMsg("message:" + msgField.getText());
					msgField.setText(null);
				}	
			}
		});

		NICKin.setText("user");
		IPin.setText("127.0.0.1");
		PORTin.setText("4221");
		
		CHAT = createTextArea("CHAT_Area", false);
		CHAT.setLineWrap(true);
		CHAT.setWrapStyleWord(true);
		CHAT.setCaretPosition(CHAT.getText().length());
		
		JPanel topPanel = new JPanel();
		topPanel.add(new JLabel("Nickname"));
		topPanel.add(NICKin);
		topPanel.add(new JLabel("IP"));
		topPanel.add(IPin);
		topPanel.add(new JLabel("PORT"));
		topPanel.add(PORTin);
		topPanel.add(mainButton);

		JPanel downPanel = new JPanel();
		downPanel.add(msgField);
		downPanel.add(sendButton);

		getContentPane().add(topPanel, BorderLayout.NORTH);
		getContentPane().add(new JScrollPane(CHAT), BorderLayout.CENTER);
		getContentPane().add(userlist, BorderLayout.EAST);
		getContentPane().add(downPanel, BorderLayout.SOUTH);

		setVisible(true);

	}

	/*
	 * 
	 */
	private static JTextField createTextField(String textFieldName,	int columns, boolean editable) {
		JTextField textField = new JTextField();
		textField.setName(textFieldName);
		textField.setColumns(columns);
		textField.setEditable(editable);
		return textField;
	}

	private static JTextArea createTextArea(String textAreaName, boolean editable) {
		JTextArea textArea = new JTextArea();
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		textArea.setName(textAreaName);
		textArea.setEditable(editable);

		return textArea;
	}

	private static JButton createButton(String buttonName) {
		JButton button = new JButton(buttonName);
		button.setName(buttonName);
		return button;
	}

	private static void buttonListener(final JButton button) {
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				switch (button.getName()) {
				case "Connect":
					if (IPin.getText().length() != 0 && PORTin.getText().length() != 0) {
						mainButton.setName("Disconnect");
						mainButton.setText("Disconnect");
						clientConnect();
					} else {
						clientDisconnect(false);
					}
					break;
				case "Disconnect":
					mainButton.setName("Connect");
					mainButton.setText("Connect");
					clientDisconnect(false);
					break;

				case "Send":
					if ((client != null) && (msgField.getText().length() != 0) && client.isConnected()) {
						Client.sendMsg("message:" + msgField.getText());
						msgField.setText(null);
					}
					break;
				default:
					JOptionPane.showMessageDialog(null,	"No action set for this button.");
				}
			}
		});
	}

	/*
	 * 
	 */
	public static void addMsg(String string) {
		CHAT.append(string);
	}
	
	public static void addMsg(char c) {
		CHAT.append(""+c);
	}

	public static void updateUserList(String[] users) {
		listModel.removeAllElements();
		for (int i = 0; i < users.length; i++) {
			listModel.addElement(users[i]);
		}
	}

	public static void clientConnect() {
		String IP = IPin.getText();
		int PORT = Integer.parseInt(PORTin.getText());
		
		try {
			client = new Client(IP, PORT);
			if (client.isConnected()) {
				client.start();
				Client.sendMsg("connect:" + NICKin.getText());
				
			}
		} catch (Exception e) {
			addMsg("Error: " + e.getMessage());
		}

	}

	public static int clientDisconnect(final boolean disconnectByServer) {
		
		ActionListener disconnect = new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				Client.disconnect(disconnectByServer);
				ActionListener close = new ActionListener() {
					public void actionPerformed(ActionEvent evt) {
						System.exit(0);
					}
				};
				new Timer(500, close).start();
			}
		};
		new Timer(500, disconnect).start();
		return 0;

	}

}