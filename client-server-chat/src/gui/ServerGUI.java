package gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

import server.Client;
import server.Server;

@SuppressWarnings("serial")
public class ServerGUI extends JFrame {

	private static Server server;
	private static JTextField PORTin;
	private static JTextArea CHAT;
	private static JButton mainButton;
	
	private static DefaultListModel<String> listModel;
	private static JList<String> userlist;
	
	public ServerGUI() {
		super("Chat Server");
		listModel = new DefaultListModel<String>();
		userlist = new JList<String>(listModel);
		
		initGUI();
	}

	private void initGUI() {
		setSize(700, 480);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {

			public void windowClosing(WindowEvent e) {
				//server.killServer();
				System.exit(0);
			}
		});

		mainButton = createButton("Start");
		buttonListener(mainButton);

		userlist.setFixedCellWidth(150);

		PORTin = createTextField("PORT_field", 8, true);
		
		PORTin.setText("4221");

		CHAT = createTextArea("CHAT_Area", false);
		CHAT.setLineWrap(true);
		CHAT.setWrapStyleWord(true);
		
		JPanel topPanel = new JPanel();
		topPanel.add(new JLabel("PORT"));
		topPanel.add(PORTin);
		topPanel.add(mainButton);

		getContentPane().add(topPanel, BorderLayout.NORTH);
		getContentPane().add(new JScrollPane(CHAT), BorderLayout.CENTER);
		getContentPane().add(userlist, BorderLayout.EAST);

		setVisible(true);

	}
	
	public static void updateUserList() {
		listModel.removeAllElements();
		for(Client c: server.defaultList.getClients()){
			listModel.addElement(c.getName());
		}
	}
	
	public static void addMsg(String string) {
		CHAT.append(string + '\n');
	}
	public static void addMsg(char c) {
		CHAT.append(""+c);
	}

	/*
	 * 
	 */
	private static JTextField createTextField(String textFieldName,	int columns, boolean editable) {
		JTextField textField = new JTextField();
		textField.setName(textFieldName);
		textField.setColumns(columns);
		textField.setEditable(editable);
		return textField;
	}

	private static JTextArea createTextArea(String textAreaName, boolean editable) {
		JTextArea textArea = new JTextArea();
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		textArea.setName(textAreaName);
		textArea.setEditable(editable);

		return textArea;
	}

	private static JButton createButton(String buttonName) {
		JButton button = new JButton(buttonName);
		button.setName(buttonName);
		return button;
	}

	private static void buttonListener(final JButton button) {
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				switch (button.getName()) {
				case "Start":
					mainButton.setName("Stop");
					mainButton.setText("Stop");
					
					server = new Server(Integer.parseInt(PORTin.getText()));
					CHAT.append("Server started");
					break;
				case "Stop":
					
					server.killServer();
					break;
				default:
					JOptionPane.showMessageDialog(null,	"No action set for this button.");
				}
			}
		});
	}

}