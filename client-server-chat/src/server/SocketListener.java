package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author menzel
 *
 * Handles new connections to start the communication 
 * builds the client objects and starts their threads
 * everything is then handed over to TeilnehmerListe for further usage
 */
public class SocketListener implements Runnable {

	private ServerSocket sSocket;
	private Socket cSocket;
	private TeilnehmerListe defaultList;
	private Server server;

	/**
	 * Constructor
	 * @param socket - Socket to listen on
	 * @param defaultList - list of clients
	 * @param server- server on which the chat is running 
	 */
	public SocketListener(ServerSocket socket, TeilnehmerListe defaultList, Server server) {
		this.sSocket = socket;
		this.defaultList = defaultList;
		this.server = server;
	}

	/**
	 * Thread run
	 * waites for new connections
	 * hadles every new connection 
	 */
	public void run(){

		while(true){
			try {

				server.printMessage("Listener is listening");
		//		sSocket.setSoTimeout(1000);
				cSocket = sSocket.accept();

				if(cSocket.isConnected()){
					connectNewClient().start();
				}

			} catch (IOException e) {
				System.err.println("Error while connecting new client");

			} catch (ClientError e){
				System.err.println("ClientError catched: " + e.getError());

			}
		}
	}

	/**
	 * Connects a new Client with the server
	 * @post - Client is connected
	 * @throws ClientError - Error if client submits an illegal argument- eg. name already in use
	 * or if list is already full
	 */
	private synchronized Thread connectNewClient() throws ClientError  {

		server.printMessage("new Client tries to connect");

		Client tmp = new Client(cSocket, server,null);
		String handshake;
		String name = "";

		handshake = tmp.getMessage();

		if(handshake.startsWith("connect:")){

			name = handshake.substring(8);
			tmp.setName(name);
			
			defaultList.checkAndAddClient(tmp);
			
			tmp.sendMessage("connect:ok");
			server.printMessage( "New Client connected with name: " + tmp.getName());
			server.publishMessage(defaultList.namelist());

			Thread t = new Thread(tmp);

			return t;


		}else{
			tmp.disconnectClient();
			throw new ClientError("wrong_handshake :" + tmp.getLastMessage()) ;
		}
	}
}