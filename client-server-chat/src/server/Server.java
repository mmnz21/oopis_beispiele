package server;

import gui.ServerGUI;

import java.io.*;
import java.net.*;

/**
 * 
 * @author menzel
 * represents a server object, constructor starts everything that is needed.
 * controll of server goes thru this object, eg sending message, or close clients.
 * This server then calls the needed methods. 
 * 
 * use ServerInterface for connection to a GUI
 *
 */
public class Server implements ServerInterface {

	private int port = 4221;
	private ServerSocket socket;
	public TeilnehmerListe defaultList;
	private Thread listener;
	private final int maxClients = 3;

	/**
	 * Constructor
	 * @param port - port on which the server is going to listen for new clients
	 */
	public Server(int port){
		
		/*
		 * Redirecting console output
		 */
		PrintStream p = new PrintStream(
				new OutputStream() {
					public void write( int b ) {
						ServerGUI.addMsg( (char)b );
					}
				}
		);
		
		System.setErr(p);
		System.setOut(p);
		
		this.port = port;

		startServer();
		defaultList = TeilnehmerListe.getTeilnehmerListe(this, maxClients);
		listener = new Thread(new SocketListener(socket,defaultList,this));
		listener.start();

	}


	/**
	 * starts a new Server
	 * @post Server is started, Socket is created
	 */
	@Override
	public void startServer(){

		try {
			socket  = new ServerSocket(port);
			printMessage("ServerSocket created");

		} catch (IOException e) {
			System.err.println("Error while creating serversocket, maybe another Server or Service is already running on this port.\nServer will exit");
			System.exit(0);
		}

	}

	/**
	 * Print a message on the server output
	 * @param text- message which is to be printed
	 */
	public void printMessage(String text) {

		System.out.println("Server> " + text);
	}

	/**
	 * sends a new message from a client to all clients 
	 * @param msg - message which is to be send, text including the message: prefix 
	 * a "message:" will be put in front of every message; this method is not to be used for commands
	 * use publishMessage(String) instead!
	 */
	public synchronized void publishMessage(Message msg){

		defaultList.sendToAll("message:" +msg.getClient().getName() + ":" +msg.getText().substring(8));
	}

	/**
	 * publish a message to all clients without a client name
	 * @param text - text to be send. Will be published exactly as given
	 */
	@Override
	public synchronized void publishMessage(String text) {

		defaultList.sendToAll(text);

	}


	/**
	 * Removes the given client from the list and sends  a new clientList to all other clients
	 * @param c - client which is going to be removed - can be null when client is already closed 
	 * @param ok - is false when client is closed with some kind of error, true when disconnect is initialized by client or server
	 * @post client close method is called, client is removed from server list
	 */
	public synchronized void closeClient(Client c, boolean ok){

		printMessage("Client connection is going to be closed");

		if(ok)
			c.sendMessage("disconnect:ok");
		
		defaultList.clientRemove(c);
		
	}

	/**
	 * Shuts a server down
	 * @post: all threads are closed, all clients are disconnected, socket listener is stopped; program will exit
	 * 
	 * All connections are closed hard, only use this if really needed
	 */
	@Override
	public void killServer(){

		printMessage("Server is shutting down now..");		
		publishMessage("disconnect:");
		
		this.listener.interrupt();
		
		defaultList.closeAllClients();

		System.exit(0);
	}



	/**
	 * returns the client list from TeilnehmerListe
	 * @return clientList in a string: syntax: namelist:name[Name:]
	 */
	@Override
	public String getClientList() {
		return this.defaultList.namelist();
	}
}