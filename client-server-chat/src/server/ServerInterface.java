package server;

/**
 * 
 * @author menzel
 *	See Server.java for @Javadoc
 */
public interface ServerInterface {
	
	void startServer();
	
	void killServer();
	
	void publishMessage(String text);
	
	void closeClient(Client c, boolean ok);
	
	String getClientList();
	

}
