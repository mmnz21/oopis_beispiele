package server;

/**
 * @author menzel
 * represents an error associated to a client
 *
 */
@SuppressWarnings("serial")
public class ClientError extends Exception {
	
	String err;
	
	/**
	 * Constructor
	 * @param err - text of error message
	 */
	public ClientError(String err) {
		super();
		this.err = err;

	}
	
	/**
	 * getter Error Text
	 * @return Error text
	 */
	public String getError(){
		return this.err;
	}
}
