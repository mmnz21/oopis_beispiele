package server;

/**
 * 
 * @author menzel
 *	represents a message, consisting of a text and the name of the client who wrote it
 */
public class Message {
	
	private String text;
	private Client c;
	
	/**
	 * Constructor
	 * @param text - text of message
	 * @param c - client who has send the message
	 */
	public Message(String text, Client c){
		this.text = text;
		this.c = c;
	}

	/**
	 * return the text of the message
	 * @return text of message
	 */
	public String getText(){
		return this.text;
	}
	
	/**
	 * return client who send the message
	 * @return client name
	 */
	public Client getClient(){
		return this.c;
	}

}
