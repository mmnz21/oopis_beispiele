package server;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;

/**
 * @author menzel
 * the Client class represents one client, mainly consisting of a name and a socket.
 * it handles the connection to one client, according to given protocol.
 * which means sending and getting messages when the thread is running
 *
 */
public class Client implements Runnable {

	private String name;
	@SuppressWarnings("unused")
	private InetAddress ip;
	private Socket clientSocket;
	private String lastMessage;
	private OutputStream o;
	private Server server;
	private BufferedReader br;
	private boolean endThread = false;

	/**
	 * Constructor
	 * @param c - a Socket for the new client
	 * @param server - Server for client communication
	 * @param name- name of client
	 */
	public Client(Socket c, Server server, String name){

		this.clientSocket = c;
		this.server = server;
		this.name = name;
		this.ip = c.getInetAddress();

		try {

			this.o = clientSocket.getOutputStream();
			this.br = new BufferedReader(new InputStreamReader(this.clientSocket.getInputStream()));

		} catch (IOException e) {
			System.err.println("could not establish IO stream");

		}
	}

	/**
	 * Thread run
	 * is listening on new messages and checking if client is kept alive with keep alive signal (client list)
	 */
	public  void run(){
		
		while(!endThread){
			
			lastMessage = this.getMessage();
	
			if(lastMessage == null){
				
				this.sendMessage(server.getClientList());
				continue;
				
			} else {
				
				Message msg = new Message(lastMessage, this); 

				if(msg.getText().startsWith("message:"))
					server.publishMessage(msg);
				else{
					if(msg.getText().startsWith("disconnect:")) 
						server.closeClient(this, true);
					else{
						this.sendMessage("disconnect:invalid_command");
						server.closeClient(this, false);
					}
				}
			}
		}
	}

	/**
	 * Sends a message to this client
	 * @param msg - message which is to be send, already includes username:message
	* @throws IOException
	 **/ 
	public synchronized void sendMessage(String msg){

		String text = msg + "\n";

		try {
			o.write(text.getBytes());
			
		} catch (IOException e) {
			server.closeClient(this, false);
		}
	}


	/**
	 * reads a message from the client
	 * @return- new message
	 */
	public String getMessage() {

		try {

			return  this.br.readLine();

		} catch (IOException e) {
			System.err.println("failed to get message from client");
		}
		return null;
	}

	/**
	 * getter for lastMessage
	 * @return
	 */
	public String getLastMessage(){
		return this.lastMessage;
	}

	/**
	 * Setter for Name
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;		
	}

	/**
	 * Getter name
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}


	/**
	 * Close the connection of this client
	 * @post socket is closed and thread is ended
	 */
	public synchronized void disconnectClient(){

		try {

			this.endThread = true;
			this.clientSocket.close();

		} catch (IOException e) {
			System.out.println("error while closing Client connection");

		}
	}

}
