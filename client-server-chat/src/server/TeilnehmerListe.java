package server;

import gui.ServerGUI;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author menzel
 * Handles the list of connected clients
 * Always contains all active connections
 *
 */
public class TeilnehmerListe {

	private static TeilnehmerListe instance;
	private int maxUsers = 3;
	private ArrayList<Client> clients;
	private Server server;
	
	/**
	 * private Constructor
	 */
	private TeilnehmerListe(Server server,int maxUsers){

		this.maxUsers = maxUsers;
		this.clients = new ArrayList<>();
		this.server = server;
	}
	
	/**
	 * Singelton. 
	 * returns the instance of the TeilnehmerListe, if no one exists it is created
	 * @param server - server on which it runs
	 * @param maxUsers- number of users allowed to be on the list
	 * @return instance of TeilnehmerListe
	 */
	public static TeilnehmerListe getTeilnehmerListe(Server server, int maxUsers){
		if (instance == null )
			instance = new TeilnehmerListe(server, maxUsers);
		return instance;
	}

	/**
	 * adds a Sender to the list of clients when all checks are correct
	 * @param s - Sender which is to be added to the list of clients
	 * @throws - Client error when name is in use, name is invalid or list is full
	 * @pre client s is created in SockerListener: name is set
	 */
	public synchronized void checkAndAddClient(Client s) throws ClientError{

		if (!checkClientName(s.getName())){	
			s.sendMessage("refused:name_in_use");
			s.disconnectClient();
			throw new ClientError("refused:name_in_use");
		}

		if(!checkClientNameSyntax(s.getName())){
			s.sendMessage("refused:invalid_name");
			s.disconnectClient();
			throw new ClientError("refused:invalid_name");			
		}

		if(!spaceLeft()){
			s.sendMessage("refused:list_full");
			s.disconnectClient();
			throw new ClientError("refused:list_full");
		}

		clients.add(s);
	}


	/**
	 * CheckClientName
	 * checks if client name is already in server list
	 * @param name - name of the client which is to be checked
	 * @return true if client is not on list; false otherwise
	 */
	private synchronized boolean checkClientName(String name) {

		for(Client tmp: clients){
			if(tmp.getName().equalsIgnoreCase(name))
				return false;
		}

		return true;
	}



	/**
	 * checks if there is space left for a new client
	 * @return true if  there is enough space left, false otherwise
	 */
	private synchronized  boolean spaceLeft() {
		return clients.size() < maxUsers;
	}
	
	/**
	 * checks if the syntax of a client name is good: only numbers and letters
	 * @return true if name is good; false otherwise
	 * 
	 */
	private synchronized boolean checkClientNameSyntax(String name){
		Pattern pat = Pattern.compile("[^a-z0-9]", Pattern.CASE_INSENSITIVE);
		Matcher mat = pat.matcher(name);

		if(mat.find() || name.length() > 29 ){

			return false;
		}

		return true;
	}


	/**
	 * creates and returns the list of name in usual syntax
	 * also updates the user list in GUI, since every change in client list calls this method
	 * @return list : list of all clients
	 */
	public String namelist() {

		String list = "";

		for(Client tmp: clients ){

			list += tmp.getName()+":";
		}
		
		ServerGUI.updateUserList();
		return "namelist:" + list;
	}


	/**
	 * Getter ClientList
	 * @return - clients- a list of all clients
	 */
	public synchronized ArrayList<Client> getClients() {
		return this.clients;
	}

	/**
	 * sends a message to all clients
	 * @param msg - string of the message; syntax> message:name:text
	 */
	public synchronized void sendToAll(String msg) {

		server.printMessage("trying to publish a new message > " +  msg);
		int i = 0;
		
		for(Client c: this.clients){
			i++;
			c.sendMessage(msg);
		}
		
		server.printMessage(" message has been send  to " + i + "  clients");

	}

	/**
	 * removes client from the list and sends a new contact list to all remaining clients
	 * @param c - client to remove
	 * @pre clients needs to be on the list, check before call method
	 */
	public synchronized void clientRemove(Client c) {
		
		this.clients.remove(c);
		c.disconnectClient();
		
		this.sendToAll(this.namelist());
		
	}


	
	/**
	 * Closes the connection of all clients
	 * @post all clients are removed from the list and disconnected
	 */
	public synchronized void closeAllClients() {
		
		for(Iterator<Client> i = clients.iterator(); i.hasNext();){
		
			Client c = (Client) i.next();
		
			this.clients.remove(c);
			c.sendMessage("disconnect:");
			c.disconnectClient();
			
			i = clients.iterator();// update iterator after a client has been removed.
			
		}
		
	}
}