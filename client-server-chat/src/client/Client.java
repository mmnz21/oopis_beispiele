package client;

import gui.ClientGUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.*;
import java.util.Arrays;

import javax.swing.Timer;

/**
 * 
 * @author charos
 * Chat client based on socket protocol for connections.
 * Created by the Chat client GUI.
 * 
 */
public class Client extends Thread {

	private static Socket cSocket;
	private static PrintWriter msgOut;
	private static BufferedReader msgIn;

	private static String msg;
	
	private static boolean connected = false;
	private static boolean exit = false;

	/**
	 * Constructor
	 * Creates the BufferedReader and PrintWriter by In-/Outputstreams of the socket. 
	 * Connects to a server
	 * 
	 * @param Ip : server ip
	 * @param Port : server port
	 * @pre ClientGUI is started
	 * @post Client is connected to a server
	 * 
	 */
	public Client(String Ip, int Port) {
		
		/*
		 * Redirecting console output
		 */
		PrintStream p = new PrintStream(
				new OutputStream() {
					public void write( int b ) {
						ClientGUI.addMsg( (char)b );
					}
				}
		);
		
		System.setErr(p);
		System.setOut(p);
		
		try {
			cSocket = new Socket(Ip, Port);
			connected = true;
			exit = false;
			
			msgIn = new BufferedReader(new InputStreamReader(cSocket.getInputStream()));
			msgOut = new PrintWriter(new OutputStreamWriter(cSocket.getOutputStream()));
			
		} catch (IOException e) {
			System.err.println("Error: " + e.getMessage());
		}

	}
	
	@Override
	public void run() {

		if ((msgIn != null) && (msgOut != null)) {
			while (!isInterrupted() || !exit) {
				
				/*
				 * if msg != null, msg is set and should be send to server
				 */
				if(msg != null){
					msgOut.println(msg);
					msgOut.flush();
					msg = null;
				}

				try {
					if (!exit && msgIn.ready()) {
						getMsg(msgIn.readLine());
					}
				} catch (IOException e) {
					System.err.println("Error: " + e.getMessage());
					break;
				}

				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					System.err.println("Error: " + e.getMessage());
					break;
				}
			}

			ClientGUI.clientDisconnect(false);
		}
	}
	
	/**
	 * Set up the message which have to send to server
	 * @param string
	 * @pre Client is connected to a Server
	 * @post message is ready to send
	 * 
	 */
	public static void sendMsg(String string) {
		msg = string;
	}

	/**
	 * 
	 * @param string
	 * @pre recieved a message from server
	 * @pre recieved message was splitted to String[]
	 * @post handled splitted message 
	 */
	public static void getMsg(String string) {
		String[] formatedMsg = formatMsg(string);

		/*
		 * "connect", "disconnect", "namelist", "refused", "message"
		 */
		switch (formatedMsg[0]) {
		case "connect":
			System.out.println("Connected to server: " + formatedMsg[1]);
			break;
			
		case "disconnect":
			System.out.println("Disconnected from server.");
			ClientGUI.clientDisconnect(true);
			break;
			
		case "namelist":
			String[] userlist = Arrays.copyOfRange(formatedMsg, 1, formatedMsg.length);
			ClientGUI.updateUserList(userlist);
			break;
			
		case "refused":
			System.out.println("Rejected by server: " + formatedMsg[1]);
			break;
			
		case "message":
			System.out.println(formatedMsg[1] + ": " + formatedMsg[2]);
			break;
			
		default:
			System.err.println("Message_error");
			break;
		}
	}
	
	/**
	 * Splits the recieved message on every ":" to substrings and put them into an array
	 * 
	 * @param string
	 * @pre message was recieved
	 * @post message is splitted
	 * @return formatedMsg[]
	 */
	public static String[] formatMsg(String string) {
		String[] formatedMsg = string.split(":");
		return formatedMsg;
	}

	/**
	 * Sends a disconnect to the server and closes the socket
	 * 
	 * @param disconnectByServer
	 * @pre Client is connected to a server
	 * @post Client is no longer connected to a server
	 * 
	 */
	public static void disconnect(boolean disconnectByServer) {
		if(!disconnectByServer){
			sendMsg("disconnect:");
		}
		
		ActionListener disconnect = new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				try {
					exit = true;
					connected = false;
					if (msgIn != null) {
						msgIn.close();
					}
					if (msgOut != null) {
						msgOut.close();
					}
					if (cSocket != null) {
						cSocket.close();
					}

				} catch (IOException e) {
					System.err.println("Error: " + e.getMessage());

				}
			}
		};
		new Timer(250, disconnect).start();
	}

	/**
	 * 
	 * @return boolean
	 */
	public boolean isConnected() {
		return Client.connected;
	}
}
